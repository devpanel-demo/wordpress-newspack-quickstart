/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/admin/index.js":
/*!****************************!*\
  !*** ./src/admin/index.js ***!
  \****************************/
/***/ (() => {

eval("var jQuery = window && window.jQuery;\njQuery(document).ready(function () {\n  jQuery(document).on('click', '.newspack-newsletters-notification-nag .notice-dismiss', function () {\n    var data = {\n      action: 'newspack_newsletters_activation_nag_dismissal'\n    };\n\n    var _ref = window && window.newspack_newsletters_activation_nag_dismissal_params,\n        ajaxurl = _ref.ajaxurl;\n\n    jQuery.post(ajaxurl, data, function () {\n      return null;\n    });\n  });\n});\n\n//# sourceURL=webpack://newspack-newsletters/./src/admin/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/admin/index.js"]();
/******/ 	var __webpack_export_target__ = window;
/******/ 	for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
/******/ 	if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ 	
/******/ })()
;