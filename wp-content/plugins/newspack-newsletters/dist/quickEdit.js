/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/quick-edit/index.js":
/*!*********************************!*\
  !*** ./src/quick-edit/index.js ***!
  \*********************************/
/***/ (() => {

eval("var jQuery = window && window.jQuery;\n\nif ('undefined' !== typeof inlineEditPost) {\n  // eslint-disable-next-line no-undef\n  var wp_inline_edit_function = inlineEditPost.edit; // eslint-disable-next-line no-undef\n\n  inlineEditPost.edit = function (post_id) {\n    wp_inline_edit_function.apply(this, arguments);\n    var id = 0;\n\n    if (typeof post_id === 'object') {\n      id = parseInt(this.getId(post_id));\n    }\n\n    if (id > 0) {\n      if (jQuery(\"tr#post-\".concat(id, \" .inline_data.is_public\")).data('is_public')) {\n        jQuery(\"tr#edit-\".concat(id, \" :input[name=\\\"switch_public_page\\\"]\")).prop('checked', true);\n      }\n    }\n  };\n}\n\n//# sourceURL=webpack://newspack-newsletters/./src/quick-edit/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/quick-edit/index.js"]();
/******/ 	var __webpack_export_target__ = window;
/******/ 	for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
/******/ 	if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ 	
/******/ })()
;