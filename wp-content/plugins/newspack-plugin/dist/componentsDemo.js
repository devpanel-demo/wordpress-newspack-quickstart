/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (function() { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/@wordpress/icons/build-module/library/audio.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@wordpress/icons/build-module/library/audio.js ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ \"@wordpress/element\");\n/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/primitives */ \"@wordpress/primitives\");\n/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__);\n\n\n/**\n * WordPress dependencies\n */\n\nconst audio = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.SVG, {\n  viewBox: \"0 0 24 24\",\n  xmlns: \"http://www.w3.org/2000/svg\"\n}, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.Path, {\n  d: \"M17.7 4.3c-1.2 0-2.8 0-3.8 1-.6.6-.9 1.5-.9 2.6V14c-.6-.6-1.5-1-2.5-1C8.6 13 7 14.6 7 16.5S8.6 20 10.5 20c1.5 0 2.8-1 3.3-2.3.5-.8.7-1.8.7-2.5V7.9c0-.7.2-1.2.5-1.6.6-.6 1.8-.6 2.8-.6h.3V4.3h-.4z\"\n}));\n/* harmony default export */ __webpack_exports__[\"default\"] = (audio);\n//# sourceMappingURL=audio.js.map\n\n//# sourceURL=webpack://newspack/./node_modules/@wordpress/icons/build-module/library/audio.js?");

/***/ }),

/***/ "./node_modules/@wordpress/icons/build-module/library/reusable-block.js":
/*!******************************************************************************!*\
  !*** ./node_modules/@wordpress/icons/build-module/library/reusable-block.js ***!
  \******************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ \"@wordpress/element\");\n/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/primitives */ \"@wordpress/primitives\");\n/* harmony import */ var _wordpress_primitives__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__);\n\n\n/**\n * WordPress dependencies\n */\n\nconst reusableBlock = (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.SVG, {\n  xmlns: \"http://www.w3.org/2000/svg\",\n  viewBox: \"0 0 24 24\"\n}, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_primitives__WEBPACK_IMPORTED_MODULE_1__.Path, {\n  d: \"M7 7.2h8.2L13.5 9l1.1 1.1 3.6-3.6-3.5-4-1.1 1 1.9 2.3H7c-.9 0-1.7.3-2.3.9-1.4 1.5-1.4 4.2-1.4 5.6v.2h1.5v-.3c0-1.1 0-3.5 1-4.5.3-.3.7-.5 1.2-.5zm13.8 4V11h-1.5v.3c0 1.1 0 3.5-1 4.5-.3.3-.7.5-1.3.5H8.8l1.7-1.7-1.1-1.1L5.9 17l3.5 4 1.1-1-1.9-2.3H17c.9 0 1.7-.3 2.3-.9 1.5-1.4 1.5-4.2 1.5-5.6z\"\n}));\n/* harmony default export */ __webpack_exports__[\"default\"] = (reusableBlock);\n//# sourceMappingURL=reusable-block.js.map\n\n//# sourceURL=webpack://newspack/./node_modules/@wordpress/icons/build-module/library/reusable-block.js?");

/***/ }),

/***/ "./assets/wizards/componentsDemo/index.js":
/*!************************************************!*\
  !*** ./assets/wizards/componentsDemo/index.js ***!
  \************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ \"@wordpress/element\");\n/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _shared_js_public_path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../shared/js/public-path */ \"./assets/shared/js/public-path.js\");\n/* harmony import */ var _shared_js_public_path__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_shared_js_public_path__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/i18n */ \"@wordpress/i18n\");\n/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/icons */ \"./node_modules/@wordpress/icons/build-module/library/home.js\");\n/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/icons */ \"./node_modules/@wordpress/icons/build-module/library/typography.js\");\n/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/icons */ \"./node_modules/@wordpress/icons/build-module/library/plus.js\");\n/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @wordpress/icons */ \"./node_modules/@wordpress/icons/build-module/library/reusable-block.js\");\n/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @wordpress/icons */ \"./node_modules/@wordpress/icons/build-module/library/audio.js\");\n/* harmony import */ var _components_src__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/src */ \"./assets/components/src/index.js\");\n\n\n/* eslint-disable jsx-a11y/anchor-is-valid */\n\n/**\n * Components Demo\n */\n\n/**\n * WordPress dependencies.\n */\n\n\n\nconst __ = _wordpress_i18n__WEBPACK_IMPORTED_MODULE_2__.__;\n\n/**\n * Internal dependencies.\n */\n\n\n\nclass ComponentsDemo extends _wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Component {\n  /**\n   * constructor. Demo of how the parent interacts with the components, and controls their values.\n   */\n  constructor() {\n    super(...arguments);\n    this.state = {\n      selectedPostForAutocompleteWithSuggestions: [],\n      selectedPostsForAutocompleteWithSuggestionsMultiSelect: [],\n      inputTextValue1: __('Input value', 'newspack'),\n      inputTextValue2: '',\n      inputTextValue3: '',\n      inputNumValue: 0,\n      image: null,\n      selectValue1: '2nd',\n      selectValue2: '',\n      selectValue3: '',\n      selectValues: [],\n      modalShown: false,\n      toggleGroupChecked: false,\n      color1: '#3366ff'\n    };\n  }\n  /**\n   * Render the example stub.\n   */\n\n\n  render() {\n    const {\n      selectedPostForAutocompleteWithSuggestions,\n      selectedPostsForAutocompleteWithSuggestionsMultiSelect,\n      inputTextValue1,\n      inputTextValue2,\n      inputTextValue3,\n      inputNumValue,\n      selectValue1,\n      selectValue2,\n      selectValue3,\n      modalShown,\n      actionCardToggleChecked,\n      toggleGroupChecked,\n      color1\n    } = this.state;\n    return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, newspack_aux_data.is_debug_mode && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Notice, {\n      isWarning: true,\n      className: \"newspack-wizard__above-header\",\n      noticeText: __('Newspack is in debug mode.', 'newspack')\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"div\", {\n      className: \"newspack-wizard__header\"\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"div\", {\n      className: \"newspack-wizard__header__inner\"\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"div\", {\n      className: \"newspack-wizard__title\"\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isLink: true,\n      href: newspack_urls.dashboard,\n      label: __('Return to Dashboard', 'newspack'),\n      showTooltip: true,\n      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_4__[\"default\"],\n      iconSize: 36\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.NewspackIcon, {\n      size: 36\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h1\", null, __('Components Demo', 'newspack'))))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"div\", {\n      className: \"newspack-wizard newspack-wizard__content\"\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Autocomplete with Suggestions (single-select)', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.AutocompleteWithSuggestions, {\n      label: __('Search for a post', 'newspack'),\n      help: __('Begin typing post title, click autocomplete result to select.', 'newspack'),\n      onChange: items => this.setState({\n        selectedPostForAutocompleteWithSuggestions: items\n      }),\n      selectedItems: selectedPostForAutocompleteWithSuggestions\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"hr\", null), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Autocomplete with Suggestions (multi-select)', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.AutocompleteWithSuggestions, {\n      hideHelp: true,\n      multiSelect: true,\n      label: __('Search widgets', 'newspack'),\n      help: __('Begin typing post title, click autocomplete result to select.', 'newspack'),\n      onChange: items => this.setState({\n        selectedPostsForAutocompleteWithSuggestionsMultiSelect: items\n      }),\n      postTypes: [{\n        slug: 'page',\n        label: 'Pages'\n      }, {\n        slug: 'post',\n        label: 'Posts'\n      }],\n      postTypeLabel: 'widget',\n      postTypeLabelPlural: 'widgets',\n      selectedItems: selectedPostsForAutocompleteWithSuggestionsMultiSelect\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Plugin toggles', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.PluginToggle, {\n      plugins: {\n        woocommerce: {\n          shouldRefreshAfterUpdate: true\n        },\n        'fb-instant-articles': {\n          actionText: __('Configure Instant Articles'),\n          href: '/wp-admin/admin.php?page=newspack'\n        }\n      }\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Web Previews', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, {\n      buttonsCard: true,\n      noBorder: true\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.WebPreview, {\n      url: \"//newspack.pub/\",\n      label: __('Preview Newspack Blog', 'newspack'),\n      isPrimary: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.WebPreview, {\n      url: \"//newspack.pub/\",\n      renderButton: _ref => {\n        let {\n          showPreview\n        } = _ref;\n        return (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"a\", {\n          href: \"#\",\n          onClick: showPreview\n        }, __('Preview Newspack Blog', 'newspack'));\n      }\n    }))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Color picker', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ColorPicker, {\n      label: __('Color Picker', 'newspack'),\n      color: color1,\n      onChange: color => this.setState({\n        color1: color\n      })\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ToggleGroup, {\n      title: __('Example Toggle Group', 'newspack'),\n      description: __('This is the description of a toggle group.', 'newspack'),\n      checked: toggleGroupChecked,\n      onChange: checked => this.setState({\n        toggleGroupChecked: checked\n      })\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"p\", null, __('This is the content of the toggle group', 'newspack')))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Handoff Buttons', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, {\n      buttonsCard: true,\n      noBorder: true\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Handoff, {\n      modalTitle: __('Manage AMP', 'newspack'),\n      modalBody: __('Click to go to the AMP dashboard. There will be a notification bar at the top with a link to return to Newspack.', 'newspack'),\n      plugin: \"amp\",\n      isTertiary: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Handoff, {\n      plugin: \"jetpack\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Handoff, {\n      plugin: \"google-site-kit\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Handoff, {\n      plugin: \"woocommerce\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Handoff, {\n      plugin: \"wordpress-seo\",\n      isPrimary: true,\n      editLink: \"/wp-admin/admin.php?page=wpseo_dashboard#top#features\"\n    }, __('Specific Yoast Page', 'newspack')))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Modal', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, {\n      buttonsCard: true,\n      noBorder: true\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isPrimary: true,\n      onClick: () => this.setState({\n        modalShown: true\n      })\n    }, __('Open modal', 'newspack'))), modalShown && (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Modal, {\n      title: __('This is the modal title', 'newspack'),\n      onRequestClose: () => this.setState({\n        modalShown: false\n      })\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"p\", null, __('Based on industry research, we advise to test the modal component, and continuing this sentence so we can see how the text wraps is one good way of doing that.', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, {\n      buttonsCard: true,\n      noBorder: true,\n      className: \"justify-end\"\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isPrimary: true,\n      onClick: () => this.setState({\n        modalShown: false\n      })\n    }, __('Dismiss', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isSecondary: true,\n      onClick: () => this.setState({\n        modalShown: false\n      })\n    }, __('Also dismiss', 'newspack'))))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Notice', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Notice, {\n      noticeText: __('This is an info notice.', 'newspack')\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Notice, {\n      noticeText: __('This is an error notice.', 'newspack'),\n      isError: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Notice, {\n      noticeText: __('This is a help notice.', 'newspack'),\n      isHelp: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Notice, {\n      noticeText: __('This is a success notice.', 'newspack'),\n      isSuccess: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Notice, {\n      noticeText: __('This is a warning notice.', 'newspack'),\n      isWarning: true\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Plugin installer', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.PluginInstaller, {\n      plugins: ['woocommerce', 'amp', 'wordpress-seo'],\n      canUninstall: true,\n      onStatus: _ref2 => {\n        let {\n          complete,\n          pluginInfo\n        } = _ref2;\n        console.log(complete ? 'All plugins installed successfully' : 'Plugin installation incomplete', pluginInfo);\n      }\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Plugin installer (small)', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.PluginInstaller, {\n      plugins: ['woocommerce', 'amp', 'wordpress-seo'],\n      isSmall: true,\n      canUninstall: true,\n      onStatus: _ref3 => {\n        let {\n          complete,\n          pluginInfo\n        } = _ref3;\n        console.log(complete ? 'All plugins installed successfully' : 'Plugin installation incomplete', pluginInfo);\n      }\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.PluginInstaller, {\n      plugins: ['woocommerce', 'amp', 'wordpress-seo'],\n      onStatus: _ref4 => {\n        let {\n          complete,\n          pluginInfo\n        } = _ref4;\n        console.log(complete ? 'All plugins installed successfully' : 'Plugin installation incomplete', pluginInfo);\n      }\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.PluginInstaller, {\n      plugins: ['woocommerce', 'amp', 'wordpress-seo'],\n      isSmall: true,\n      onStatus: _ref5 => {\n        let {\n          complete,\n          pluginInfo\n        } = _ref5;\n        console.log(complete ? 'All plugins installed successfully' : 'Plugin installation incomplete', pluginInfo);\n      }\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example One', 'newspack'),\n      description: __('Has an action button.', 'newspack'),\n      actionText: __('Install', 'newspack'),\n      onClick: () => {\n        console.log('Install clicked');\n      }\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Two', 'newspack'),\n      description: __('Has action button and secondary button (visible on hover).', 'newspack'),\n      actionText: __('Edit', 'newspack'),\n      secondaryActionText: __('Delete', 'newspack'),\n      onClick: () => {\n        console.log('Edit clicked');\n      },\n      onSecondaryActionClick: () => {\n        console.log('Delete clicked');\n      }\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Three', 'newspack'),\n      description: __('Waiting/in-progress state, no action button.', 'newspack'),\n      actionText: __('Installing…', 'newspack'),\n      isWaiting: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Four', 'newspack'),\n      description: __('Error notification', 'newspack'),\n      actionText: __('Install', 'newspack'),\n      onClick: () => {\n        console.log('Install clicked');\n      },\n      notification: (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, \"Plugin cannot be installed \", (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"a\", {\n        href: \"#\"\n      }, \"Retry\"), \" | \", (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"a\", {\n        href: \"#\"\n      }, \"Documentation\")),\n      notificationLevel: \"error\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Five', 'newspack'),\n      description: __('Warning notification, action button', 'newspack'),\n      notification: (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, \"There is a new version available. \", (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"a\", {\n        href: \"#\"\n      }, \"View details\"), \" or\", ' ', (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"a\", {\n        href: \"#\"\n      }, \"update now\")),\n      notificationLevel: \"warning\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Six', 'newspack'),\n      description: __('Static text, no button', 'newspack'),\n      actionText: __('Active', 'newspack')\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Seven', 'newspack'),\n      description: __('Static text, secondary action button.', 'newspack'),\n      actionText: __('Active', 'newspack'),\n      secondaryActionText: __('Delete', 'newspack'),\n      onSecondaryActionClick: () => {\n        console.log('Delete clicked');\n      }\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Eight', 'newspack'),\n      description: __('Image with link and action button.', 'newspack'),\n      actionText: __('Configure', 'newspack'),\n      onClick: () => {\n        console.log('Configure clicked');\n      },\n      image: \"https://i0.wp.com/newspack.pub/wp-content/uploads/2020/06/pexels-photo-3183150.jpeg\",\n      imageLink: \"https://newspack.pub\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Nine', 'newspack'),\n      description: __('Action Card with Toggle Control.', 'newspack'),\n      actionText: actionCardToggleChecked && __('Configure', 'newspack'),\n      onClick: () => {\n        console.log('Configure clicked');\n      },\n      toggleOnChange: checked => this.setState({\n        actionCardToggleChecked: checked\n      }),\n      toggleChecked: actionCardToggleChecked\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      badge: __('Premium', 'newspack'),\n      title: __('Example Ten', 'newspack'),\n      description: __('An example of an action card with a badge.', 'newspack'),\n      actionText: __('Install', 'newspack'),\n      onClick: () => {\n        console.log('Install clicked');\n      }\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      isSmall: true,\n      title: __('Example Eleven', 'newspack'),\n      description: __('An example of a small action card.', 'newspack'),\n      actionText: __('Installing', 'newspack'),\n      onClick: () => {\n        console.log('Install clicked');\n      }\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Twelve', 'newspack'),\n      description: __('Action card with an unchecked checkbox.', 'newspack'),\n      actionText: __('Configure', 'newspack'),\n      onClick: () => {\n        console.log('Configure');\n      },\n      checkbox: \"unchecked\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Example Thirteen', 'newspack'),\n      description: __('Action card with a checked checkbox.', 'newspack'),\n      secondaryActionText: __('Disconnect', 'newspack'),\n      onSecondaryActionClick: () => {\n        console.log('Disconnect');\n      },\n      checkbox: \"checked\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      badge: [__('Premium', 'newspack'), __('Archived', 'newspack')],\n      title: __('Example Fourteen', 'newspack'),\n      description: __('An example of an action card with two badges.', 'newspack'),\n      actionText: __('Install', 'newspack'),\n      onClick: () => {\n        console.log('Install clicked');\n      }\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Handoff', 'newspack'),\n      description: __('An example of an action card with Handoff.', 'newspack'),\n      actionText: __('Configure', 'newspack'),\n      handoff: \"jetpack\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ActionCard, {\n      title: __('Handoff', 'newspack'),\n      description: __(' An example of an action card with Handoff and EditLink.', 'newspack'),\n      actionText: __('Configure', 'newspack'),\n      handoff: \"jetpack\",\n      editLink: \"admin.php?page=jetpack#/settings\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Checkboxes', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.CheckboxControl, {\n      label: __('Checkbox is tested?'),\n      onChange: function () {\n        console.log(\"Yep, it's tested\");\n      }\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.CheckboxControl, {\n      label: __('Checkbox w/Tooltip', 'newspack'),\n      onChange: function () {\n        console.log(\"Yep, it's tested\");\n      },\n      tooltip: __('This is the tooltip text', 'newspack')\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.CheckboxControl, {\n      label: __('Checkbox w/Help', 'newspack'),\n      onChange: function () {\n        console.log(\"Yep, it's tested\");\n      },\n      help: __('This is the help text', 'newspack')\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Image Uploader', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ImageUpload, {\n      image: this.state.image,\n      onChange: image => {\n        this.setState({\n          image\n        });\n        console.log('Image:');\n        console.log(image);\n      }\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Text Inputs', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.TextControl, {\n      label: __('Text Input with value', 'newspack'),\n      value: inputTextValue1,\n      onChange: value => this.setState({\n        inputTextValue1: value\n      })\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.TextControl, {\n      label: __('Text Input empty', 'newspack'),\n      value: inputTextValue2,\n      onChange: value => this.setState({\n        inputTextValue2: value\n      })\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.TextControl, {\n      type: \"number\",\n      label: __('Number Input', 'newspack'),\n      value: inputNumValue,\n      onChange: value => this.setState({\n        inputNumValue: value\n      })\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.TextControl, {\n      label: __('Text Input disabled', 'newspack'),\n      disabled: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.TextControl, {\n      label: __('Small', 'newspack'),\n      value: inputTextValue3,\n      isSmall: true,\n      onChange: value => this.setState({\n        inputTextValue3: value\n      })\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Progress bar', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ProgressBar, {\n      completed: \"2\",\n      total: \"3\"\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ProgressBar, {\n      completed: \"2\",\n      total: \"5\",\n      label: __('Progress made', 'newspack')\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ProgressBar, {\n      completed: \"0\",\n      total: \"5\",\n      displayFraction: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ProgressBar, {\n      completed: \"3\",\n      total: \"8\",\n      label: __('Progress made', 'newspack'),\n      displayFraction: true\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Select dropdowns', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.SelectControl, {\n      label: __('Label for Select with a preselection', 'newspack'),\n      value: selectValue1,\n      options: [{\n        value: null,\n        label: __('- Select -', 'newspack'),\n        disabled: true\n      }, {\n        value: '1st',\n        label: __('First', 'newspack')\n      }, {\n        value: '2nd',\n        label: __('Second', 'newspack')\n      }, {\n        value: '3rd',\n        label: __('Third', 'newspack')\n      }],\n      onChange: value => this.setState({\n        selectValue1: value\n      })\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.SelectControl, {\n      label: __('Label for Select with no preselection', 'newspack'),\n      value: selectValue2,\n      options: [{\n        value: null,\n        label: __('- Select -', 'newspack'),\n        disabled: true\n      }, {\n        value: '1st',\n        label: __('First', 'newspack')\n      }, {\n        value: '2nd',\n        label: __('Second', 'newspack')\n      }, {\n        value: '3rd',\n        label: __('Third', 'newspack')\n      }],\n      onChange: value => this.setState({\n        selectValue2: value\n      })\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.SelectControl, {\n      label: __('Label for disabled Select', 'newspack'),\n      disabled: true,\n      options: [{\n        value: null,\n        label: __('- Select -', 'newspack'),\n        disabled: true\n      }, {\n        value: '1st',\n        label: __('First', 'newspack')\n      }, {\n        value: '2nd',\n        label: __('Second', 'newspack')\n      }, {\n        value: '3rd',\n        label: __('Third', 'newspack')\n      }]\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.SelectControl, {\n      label: __('Small', 'newspack'),\n      value: selectValue3,\n      isSmall: true,\n      options: [{\n        value: null,\n        label: __('- Select -', 'newspack'),\n        disabled: true\n      }, {\n        value: '1st',\n        label: __('First', 'newspack')\n      }, {\n        value: '2nd',\n        label: __('Second', 'newspack')\n      }, {\n        value: '3rd',\n        label: __('Third', 'newspack')\n      }],\n      onChange: value => this.setState({\n        selectValue3: value\n      })\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.SelectControl, {\n      multiple: true,\n      label: __('Multi-select', 'newspack'),\n      value: this.state.selectValues,\n      options: [{\n        value: '1st',\n        label: __('First', 'newspack')\n      }, {\n        value: '2nd',\n        label: __('Second', 'newspack')\n      }, {\n        value: '3rd',\n        label: __('Third', 'newspack')\n      }, {\n        value: '4th',\n        label: __('Fourth', 'newspack')\n      }, {\n        value: '5th',\n        label: __('Fifth', 'newspack')\n      }, {\n        value: '6th',\n        label: __('Sixth', 'newspack')\n      }, {\n        value: '7th',\n        label: __('Seventh', 'newspack')\n      }],\n      onChange: selectValues => this.setState({\n        selectValues\n      })\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Notice, {\n      noticeText: (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.Fragment, null, __('Selected:', 'newspack'), ' ', this.state.selectValues.length > 0 ? this.state.selectValues.join(', ') : __('none', 'newspack'))\n    })), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, __('Buttons', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h3\", null, __('Default', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, {\n      buttonsCard: true,\n      noBorder: true\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isPrimary: true\n    }, \"isPrimary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isSecondary: true\n    }, \"isSecondary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isTertiary: true\n    }, \"isTertiary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isQuaternary: true\n    }, \"isQuaternary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isLink: true\n    }, \"isLink\")), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h3\", null, __('Disabled', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, {\n      buttonsCard: true,\n      noBorder: true\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isPrimary: true,\n      disabled: true\n    }, \"isPrimary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isSecondary: true,\n      disabled: true\n    }, \"isSecondary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isTertiary: true,\n      disabled: true\n    }, \"isTertiary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isQuaternary: true,\n      disabled: true\n    }, \"isQuaternary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isLink: true,\n      disabled: true\n    }, \"isLink\")), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h3\", null, __('Small', 'newspack')), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, {\n      buttonsCard: true,\n      noBorder: true\n    }, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isPrimary: true,\n      isSmall: true\n    }, \"isPrimary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isSecondary: true,\n      isSmall: true\n    }, \"isSecondary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isTertiary: true,\n      isSmall: true\n    }, \"isTertiary\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Button, {\n      isQuaternary: true,\n      isSmall: true\n    }, \"isQuaternary\"))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Card, null, (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(\"h2\", null, \"ButtonCard\"), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ButtonCard, {\n      href: \"admin.php?page=newspack-site-design-wizard\",\n      title: __('Site Design', 'newspack'),\n      desc: __('Branding, color, typography, layouts', 'newspack'),\n      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_5__[\"default\"],\n      chevron: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ButtonCard, {\n      href: \"#\",\n      title: __('Start a new site', 'newspack'),\n      desc: __(\"You don't have content to import\", 'newspack'),\n      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_6__[\"default\"],\n      className: \"br--top\",\n      grouped: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ButtonCard, {\n      href: \"#\",\n      title: __('Migrate an existing site', 'newspack'),\n      desc: __('You have content to import', 'newspack'),\n      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_7__[\"default\"],\n      className: \"br--bottom\",\n      grouped: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ButtonCard, {\n      href: \"#\",\n      title: __('Add a new Podcast', 'newspack'),\n      desc: ('Small', 'newspack'),\n      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_8__[\"default\"],\n      className: \"br--top\",\n      isSmall: true,\n      grouped: true\n    }), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.ButtonCard, {\n      href: \"#\",\n      title: __('Add a new Font', 'newspack'),\n      desc: ('Small + chevron', 'newspack'),\n      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_5__[\"default\"],\n      className: \"br--bottom\",\n      chevron: true,\n      isSmall: true,\n      grouped: true\n    }))), (0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(_components_src__WEBPACK_IMPORTED_MODULE_3__.Footer, null));\n  }\n\n}\n\n(0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.render)((0,_wordpress_element__WEBPACK_IMPORTED_MODULE_0__.createElement)(ComponentsDemo, null), document.getElementById('newspack-components-demo'));\n\n//# sourceURL=webpack://newspack/./assets/wizards/componentsDemo/index.js?");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "React" ***!
  \************************/
/***/ (function(module) {

module.exports = window["React"];

/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/***/ (function(module) {

module.exports = window["lodash"];

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/***/ (function(module) {

module.exports = window["moment"];

/***/ }),

/***/ "@wordpress/api-fetch":
/*!**********************************!*\
  !*** external ["wp","apiFetch"] ***!
  \**********************************/
/***/ (function(module) {

module.exports = window["wp"]["apiFetch"];

/***/ }),

/***/ "@wordpress/components":
/*!************************************!*\
  !*** external ["wp","components"] ***!
  \************************************/
/***/ (function(module) {

module.exports = window["wp"]["components"];

/***/ }),

/***/ "@wordpress/element":
/*!*********************************!*\
  !*** external ["wp","element"] ***!
  \*********************************/
/***/ (function(module) {

module.exports = window["wp"]["element"];

/***/ }),

/***/ "@wordpress/html-entities":
/*!**************************************!*\
  !*** external ["wp","htmlEntities"] ***!
  \**************************************/
/***/ (function(module) {

module.exports = window["wp"]["htmlEntities"];

/***/ }),

/***/ "@wordpress/i18n":
/*!******************************!*\
  !*** external ["wp","i18n"] ***!
  \******************************/
/***/ (function(module) {

module.exports = window["wp"]["i18n"];

/***/ }),

/***/ "@wordpress/keycodes":
/*!**********************************!*\
  !*** external ["wp","keycodes"] ***!
  \**********************************/
/***/ (function(module) {

module.exports = window["wp"]["keycodes"];

/***/ }),

/***/ "@wordpress/primitives":
/*!************************************!*\
  !*** external ["wp","primitives"] ***!
  \************************************/
/***/ (function(module) {

module.exports = window["wp"]["primitives"];

/***/ }),

/***/ "@wordpress/url":
/*!*****************************!*\
  !*** external ["wp","url"] ***!
  \*****************************/
/***/ (function(module) {

module.exports = window["wp"]["url"];

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	!function() {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	!function() {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"componentsDemo": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkwebpack"] = self["webpackChunkwebpack"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["commons"], function() { return __webpack_require__("./assets/wizards/componentsDemo/index.js"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	var __webpack_export_target__ = window;
/******/ 	for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
/******/ 	if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ 	
/******/ })()
;